#ifndef TESSELATION_H
#define TESSELATION_H

#include <QWidget>
#include <QPainter>

class Tessellation : public QWidget
{
    Q_OBJECT
public:
    explicit Tessellation(QWidget *parent = 0);
    int getRotation();
    int getWidth();
    int getHeight();

    void setRotation(int r);
    void setWidth(int w);
    void setHeight(int h);


    void setParent(QWidget *p) {this->setParent(p);}

signals:

public slots:

protected:
    void paintEvent(QPaintEvent *event);
private:
    int rotation;
    int width, height;
};

#endif // TESSELATION_H
