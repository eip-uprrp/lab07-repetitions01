
#include <QApplication>
#include <iostream>
#include <QLineEdit>
#include <QObject>
#include <QAction>
#include <QPushButton>
#include <cmath>
#include <QDebug>

#include "drawingWindow.h"
#include "tessellation.h"

// These are some of the tessellation patterns in:
// https://sites.google.com/a/wellesley.edu/wellesley-cs118-spring13/lectures-labs/lab-2

void foo(DrawingWindow &w) {
    int rot = 0;
    for (int i = 0; i < 4; i++) {
        Tessellation t;
        t.setRotation(rot);
        t.move(i * 50, i * 50);
        w.addTessellation(t);
        rot += 90;
    }
}

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    DrawingWindow w;

    w.resize(400, 400);
    w.show();
    foo(w);

    return a.exec();

}


