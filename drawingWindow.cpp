#include "drawingWindow.h"
#include "ui_drawingWindow.h"
#include <vector>

///
/// \brief DrawingWindow::DrawingWindow - Constructor.
/// \param parent - when creating a DrawingWindow in the main function, leave this parameter empty
///

DrawingWindow::DrawingWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DrawingWindow)
{
    ui->setupUi(this);
    myTessellation = new vector<Tessellation *>;
    vT = new vector<Tessellation *>;
    vL = new vector<Line *>;

    // just hidding the toolbars to have a bigger drawing area.
    QList<QToolBar *> toolbars = this->findChildren<QToolBar *>();
    foreach(QToolBar *t, toolbars) t->hide();
    QList<QStatusBar *> statusbars = this->findChildren<QStatusBar *>();
    foreach(QStatusBar *t, statusbars) t->hide();
}

///
/// \brief DrawingWindow::addLine - Add a line to the window, specifying coordinates
/// of the starting and end points.
/// \param x0 - starting x
/// \param y0 - starting y
/// \param x1 - end x
/// \param y1 - end y
/// \param width - line width
/// \param color - line color
///

void DrawingWindow::addLine(int x0, int y0, int x1, int y1, int width, QColor color) {
    Line *tmp = new Line(x0,y0,x1,y1, width, color, this);
    tmp->show();
    vL->push_back(tmp);
}

///
/// \brief DrawingWindow::addLinePolar- Add a line to the window, specifying coordinates
/// of the starting point, the length and angle.
/// \param x0 - starting x
/// \param y0 - starting y
/// \param length - length of the line
/// \param angle - angle
/// \param width - line width
/// \param color - line color
///
///
void DrawingWindow::addLinePolar(int x0, int y0, int length, double angle, int width, QColor color) {
    Line *tmp = new Line(x0,y0,length, angle, width, color, this);
    tmp->show();
    vL->push_back(tmp);
}



///
/// \brief DrawingWindow::addTessellation - add a tessalation to the window
/// \param t - a tessellation object
///

void DrawingWindow::addTessellation(Tessellation &t) {
    Tessellation *tmp = new Tessellation(this);
    tmp->move(t.x(),t.y());
    tmp->setRotation(t.getRotation());
    tmp->show();
    vT->push_back(tmp);
}

///
/// \brief DrawingWindow::~DrawingWindow - the destructor
///

DrawingWindow::~DrawingWindow()
{
    foreach(Tessellation *t, *vT) delete t;
    delete vT;
    foreach(Line *l, *vL) delete l;
    delete vL;
    delete ui;
}


///
/// \brief DrawingWindow::paintEvent
///

void DrawingWindow::paintEvent(QPaintEvent *) {
    QPainter p(this);
    QPen myPen;
    myPen.setWidth(1);
    myPen.setColor(QColor(0x0000ff));

    p.setPen(myPen);
    p.setBrush(Qt::cyan);

}
