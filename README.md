#Lab. 7: Estructuras de repetición

Una de las ventajas de utilizar programas de computadoras es que podemos realizar tareas repetitivas facilmente. Los ciclos como `for`, `while`, y `do-while` son un tipo de estructura de control que nos permiten repetir un conjunto de instrucciones. A estas estructuras también les llamamos *estructuras de repetición*. 


#Objetivos:

Al finalizar la experiencia de laboratorio de hoy los estudiantes habrán practicado el uso de estructuras de repetición para producir patrones. También practicarán el uso de funciones y objetos.

Esta experiencia de laboratorio es una adaptación de https://sites.google.com/a/wellesley.edu/wellesley-cs118-spring13/lectures-labs/lab-2.

#Pre-Lab:

Antes de llegar al laboratorio cada estudiante debe:

1. haber repasado los conceptos básicos relacionados a estructuras de repetición.

2. haber estudiado los conceptos e instrucciones para la sesión de laboratorio, especialmente el uso de los métodos para ajustar el tamaño de las ventanas, colocar las teselaciones en posiciones específicas y rotar teselaciones.

3. haber tomado el [quiz Pre-Lab 7](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6752) (recuerda que el quiz Pre-Lab puede tener conceptos explicados en las instrucciones del laboratorio).

#Teselaciones

Una teselación ("tessellation" o "tilling") es un mosaico que se crea repitiendo una figura para cubrir una superficie sin dejar huecos y sin solapamientos. Una *teselación regular* es una figura que se forma repitiendo un mismo *polígono regular*, como triángulos, cuadrados o hexágonos. (Un polígono regular es un polígono todos sus lados son congruentes y los ángulos que se forman con los lados son congruentes.)  



![http://www.mathnstuff.com/math/spoken/here/2class/150/regular.gif](http://www.mathnstuff.com/math/spoken/here/2class/150/regular.gif)

**Figura 1** - Las únicas teselaciones regulares posibles obtenidas usando triángulos, cuadrados o hexágonos.

En esta experiencia de laboratorio usaremos teselaciones de cuadrados para practicar el uso de ciclos anidados para producir patrones.

##Biblioteca

El proyecto disponible en `https://bitbucket.org/eip-uprrp/lab07-Repetition1.git` contiene la clase `Tessellation`, que es una abstracción de una teselación con cuadrados, y la clase `DrawingWindow`. El siguiente código de muestra crea un `DrawingWindow` llamado `w` y un `Tessellation` llamado `t`. Nota que el método `addTessellation` de la clase `DrawingWindow` debe ser invocado para que la teselación se dibuje.


```cpp
int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    DrawingWindow w;        // crea el objeto w de la clase DrawingWindow 
    w.resize(300, 300);
    w.show();

    Tessellation t;          // crea el objeto t de la clase Tessellation 
    t.move(50,100);         // dice la posición de la teselacion

    w.addTessellation(t);    // anade la teselacion a la pantalla

    return a.exec();
}
```
**Código 1: Código en función main para crear ventana y teselación**

La pantalla que se obtendrá es similar a la siguiente:

<div align='center'><img src="http://i.imgur.com/JJwWlWl.png"></div>


Lo que sigue es un ejemplo de cómo crear una función para dibujar cuatro treselaciones en las posiciones  (0,0), (50,50), (100,100), and (150,150), con rotaciones de la figura original de  0, 90, 180 y 270 grados a favor de las manecillas del reloj. 

```cpp
int foo(DrawingWindow& w) {
    int rot = 0;
    for (int i = 0; i < 4; i++) {
        Tessellation t;
        t.setRotation(rot);
        t.move(i * 50, i * 50);
        w.addTessellation(t);
        rot += 90;
    }

}

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    DrawingWindow w;
    w.resize(300, 300);
    w.show();

    foo(w);
    return a.exec();
}
```
**Código 2: Función que dibuja teselaciones**

Observa como la función `foo` necesita recibir una referencia al objeto de la clase `DrawingWindow` ya que está invocando el método `addTessellation`  de esta clase en cada iteración del ciclo.

La figura que se obtiene es similar a la figura que está antes del Ejercicio 1, abajo.


#Sesión de laboratorio:

En la experiencia de laboratorio de hoy practicarás el uso de estructuras de repetición para crear distintas teselaciones.

##**Instrucciones:**

1.  Ve a la pantalla de terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/lab07-Repetition1.git` para descargar la carpeta `Lab07-Repetition1` a tu computadora.

2.  Marca doble "click" en el archivo `Tessellations.pro` para cargar este proyecto a Qt. El proyecto `Tessellations.pro` contiene las clases `Tessellation` y `DrawinfWindow` y la función `foo`. 

3. Corre el programa. Debes observar la figura de abajo.

<div align='center'><img src="http://i.imgur.com/GKzWNCv.png"></div>

Esta figura se crea con la función `foo` mostrada en el Código 2. Nota cómo las instrucciones en el ciclo `for` de esta función utilizan el contador del ciclo para determinar la posición del cuadrado y como se incrementa el valor de la rotación.


### **Ejercicio 1**

Crea una función  `herringbone` que produzca la siguiente teselación:

<div align='center'><img src="http://i.imgur.com/u7nLnnC.png"></div>



El tamaño de la ventana es 400x400. El tamaño de cada cuadrado de la teselación es su "default" 50x50.

Copia la función en la sección correspondiente de la página de [Entregas del Lab 7](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6753). Recuerda comentar adecuadamente la función y usar buenas prácticas de indentación y selección de nombres para las variables.


### **Ejercicio 2**

Crea una función `zigzag` que produzca la siguiente teselación:

<div align='center'><img src="http://i.imgur.com/wOS01xh.png"></div>



Copia la función en la sección correspondiente de la página de [Entregas del Lab 7](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6753). Recuerda comentar adecuadamente la función y usar buenas prácticas de indentación y selección de nombres para las variables.

### **Ejercicio 3**

Crea una función `diamond` tque produzca la siguiente teselación:

<div align='center'><img src="http://i.imgur.com/0vnI7rL.png"></div>



Copia la función en la sección correspondiente de la página de [Entregas del Lab 7](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6753). Recuerda comentar adecuadamente la función y usar buenas prácticas de indentación y selección de  nombres para las variables.

**NOTA** Además de copiar las funciones en cada una de las cajitas, debes subir un archivo con todas las funciones debidamente documentadas en [Entrega Lab 7.](http://moodle.ccom.uprrp.edu/mod/assignment/view.php?id=6756)




# Referencias

<a name="Rocha"></a>[1] Rocha, Anderson, and Siome Goldenstein. "Steganography and steganalysis in digital multimedia: Hype or hallelujah?." Revista de Informática Teórica e Aplicada 15.1 (2008): 83-110.